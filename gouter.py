import matplotlib.pyplot as plt
import numpy as np

MAX_GATEAU = 1.5
gateau = np.arange(0,MAX_GATEAU, 0.1)

jus = (800 - 864 * gateau) / 270

plt.plot(gateau, jus)
plt.fill_between(gateau, jus, 3, alpha=.5)

plt.plot(gateau, gateau)
plt.fill_between(gateau, gateau, -1.5, alpha=.5)
plt.ylabel('Prix du verre de jus')
plt.xlabel('Prix de la part de gateau')
plt.show()
